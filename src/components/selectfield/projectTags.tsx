import * as React from 'react';
import * as Parse from 'parse';
import QueryLINQ from 'berish-parse-query-linq';
import { LINQ } from 'berish-linq/dist';
import * as Model from '../../model';
import { PageController } from '../../abstract/global/pageController';
import { Tagsfield } from '../../abstract/components';
import { ISelectFieldProps } from '../../abstract/components/selectfield';
import {
  AbstractComponent,
  IFormItemProps
} from '../../abstract/components/abstract';

interface IProps extends ISelectFieldProps<Model.Team>, IFormItemProps {
  controller: PageController;
}

interface IState {
  data: LINQ<Model.Project>;
}

export default class extends AbstractComponent<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = {
      data: LINQ.fromArray([])
    };
  }

  componentDidMount() {
    this.props.controller.tryLoad(this.onLoad);
  }

  onLoad = async (nextProps?: IProps) => {
    let query = new Parse.Query(Model.Project);
    let data = await QueryLINQ(query);
    this.setState({
      data
    });
  };

  renderData = () => {
    let { data } = this.state;
    return data.select(m => {
      return {
        value: m,
        view: `${m.code} - ${m.name}`
      };
    });
  };

  render() {
    return (
      <Tagsfield
        placeholder="Выберите проекты"
        data={this.renderData()}
        {...this.props}
      />
    );
  }
}
