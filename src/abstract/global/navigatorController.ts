import { History } from 'history';
import { IContentRouteProps } from '../router/contentRoute';
import { IPageControllerProps, PageController } from './pageController';
import Portal from 'berish-react-portals';
import ModalRoute from '../router/modalRoute';

export class NavigatorController<MatchGeneric, ResolveGeneric> {
  private _history: History = null;
  private _params: MatchGeneric = null;

  constructor(contentRoute: IContentRouteProps<MatchGeneric, ResolveGeneric>) {
    this.receive(contentRoute);
  }

  receive(contentRoute: IContentRouteProps<MatchGeneric, ResolveGeneric>) {
    this._history = contentRoute.history;
    this._params = contentRoute.params;
    return this;
  }

  get params() {
    return this._params || ({} as MatchGeneric);
  }

  get location() {
    return this._history.location;
  }

  private prepareUrl(path: string, params?: { [key: string]: any }) {
    let url = Object.keys(params || {})
      .map(key => `${key}=${params[key]}`)
      .join('&');
    return `${path}${url ? '?' + url : ''}`;
  }

  push(path: string, params?: { [key: string]: any }) {
    let paramsUrl = this.prepareUrl(path, params);
    this._history.push(paramsUrl);
  }

  pushModal<MatchGeneric, ResolveGeneric>(component: React.ComponentClass<IPageControllerProps<MatchGeneric, ResolveGeneric>>) {
    let decorate = Portal.create(ModalRoute);
    return (params?: MatchGeneric) => {
      let props: IContentRouteProps<MatchGeneric, ResolveGeneric> = {
        component,
        params,
        history: this._history
      };
      return decorate(props) as Promise<ResolveGeneric>;
    };
  }

  goBack(path: string, params?: { [key: string]: any }) {
    return this.push(path, params);
  }

  replace(path: string, params?: { [key: string]: any }) {
    let paramsUrl = this.prepareUrl(path, params);
    this._history.replace(paramsUrl);
  }
}
