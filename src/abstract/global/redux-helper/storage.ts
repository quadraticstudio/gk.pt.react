import Serber from 'berish-serber';
import { TypeofSerberPluginAdapter } from 'berish-serber/dist/lib/abstract';
import { Serber as SerberClass } from 'berish-serber/dist/lib/serber';

const STORAGE_PREFIX = 'BERISH-SERBER';

export interface IStorageAdapter {
  getItem: (key: string) => Promise<any>;
  setItem: (key: string, value: any) => Promise<void>;
  removeItem: (key: string) => Promise<void>;
}

export class Storage {
  protected _storeName: string = null;
  protected _storageAdapter: IStorageAdapter = null;
  protected _serber: SerberClass = null;

  constructor(storeName: string, storageAdapter: IStorageAdapter, serberPlugins?: TypeofSerberPluginAdapter | TypeofSerberPluginAdapter[]) {
    this._storeName = storeName;
    this._storageAdapter = storageAdapter;
    this._serber = Serber.scope(this._storeName).plugin(serberPlugins || Serber.defaults);
  }

  getName() {
    return STORAGE_PREFIX + '-' + this._storeName;
  }

  async save(state: any) {
    const newState = await this._serber.serializeAsync(state);
    const newStateStringify = JSON.stringify(newState);
    return this._storageAdapter.setItem(this.getName(), newStateStringify);
  }

  async load() {
    const stateStringify = await this._storageAdapter.getItem(this.getName());
    const newState = JSON.parse(stateStringify);
    return this._serber.deserialize(newState);
  }

  async clear() {
    await this._storageAdapter.removeItem(this.getName());
  }
}
