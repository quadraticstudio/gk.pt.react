import * as React from 'react';
import * as Parse from 'parse';
import { toast } from 'react-toastify';
import { LINQ } from 'berish-linq';
import * as ringle from 'berish-ringle';
import { getError } from '../util/error';
import { Spin } from '../components';

class MessagesController {
  isParseError(error: any): error is Parse.Error {
    return error.code != null;
  }
  getParseError<T>(error: T) {
    const childError = (error: Parse.Error): Parse.Error => {
      let message = error.message as string | Parse.Error;
      if (typeof message == 'string') return error;
      return childError(message);
    };
    if (this.isParseError(error)) {
      return childError(error);
    }
    return error;
  }

  private devError(error: Error | Parse.Error | string) {
    error = this.getParseError(error);
    let messagesStrings =
      error instanceof Error
        ? [`Название ошибки: ${error.message}`, `Описание ошибки: ${error.name}`, `Стек: ${error.stack}`]
        : this.isParseError(error)
          ? [`Код ошибки: ${error.code}`, `Описание ошибки: ${getError(error.code as any)}`]
          : [`Описание ошибки: ${error}`];
    let msg = LINQ.fromArray(messagesStrings)
      .select((m, i) => <p key={i}>{m}</p>)
      .toArray();
    let content = (
      <div>
        <p>
          <b>На сервере произошла ошибка.</b>
        </p>
        <p>
          <b>Если ошибка повторяется часто, пожалуйста, сообщите технической поддержке об этом</b>
        </p>
        <div
          style={{
            border: '1px solid #404040',
            padding: '20px 10px',
            backgroundColor: 'grey'
          }}
        >
          {msg}
        </div>
      </div>
    );
    // Modal.error({
    //   title: 'Произошла ошибка',
    //   content: content,
    //   okText: 'Закрыть',
    //   width: 400
    // });
  }

  error(error: Error | Parse.Error | string) {
    // return process.env.NODE_ENV == 'development'
    //   ? this.devError(error)
    //   :
    return this.notification('error', 'Произошла ошибка', error);
  }

  notification(type: 'error' | 'success' | 'info' | 'warning', title: string, message: Error | Parse.Error | string) {
    message = this.getParseError(message);
    let msg =
      type == 'error'
        ? message instanceof Error
          ? [`Ошибка: ${message.message}`]
          : this.isParseError(message)
            ? [`Код ошибки: ${message.code}`, `Описание: ${getError(message.code as any)}`]
            : [`Описание ошибки: ${message}`]
        : [message];
    toast[type](msg.join('. '));
  }

  loadingGlobal(loading?: boolean) {
    return this.loadingJSXElement(<div style={{ width: '100vw', height: '100vh' }} />, loading);
  }

  loadingJSXElement(element: React.ReactNode | React.ReactNode[], loading?: boolean) {
    return (
      <Spin
        loading={!!loading}
        style={{
          maxHeight: '100000px',
          position: 'fixed',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0
        }}
      >
        {element}
      </Spin>
    );
  }

  loadingComponent<T>(Component: React.ComponentClass<T>) {
    let self = this;
    return class Container extends React.Component<T & { loading?: boolean }> {
      render() {
        return self.loadingJSXElement(<Component {...this.props} />, this.props.loading);
      }
    };
  }
}

export default ringle.getSingleton(MessagesController);
