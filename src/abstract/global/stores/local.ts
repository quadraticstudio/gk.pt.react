import { MethodStore } from '../redux-helper';

interface ILocalState {
  collapsed: boolean;
  currentKey: string;
  isAdmin: boolean;
}

export class LocalStore extends MethodStore<ILocalState> {
  get collapsed() {
    return this.get('collapsed');
  }

  set collapsed(value: boolean) {
    this.set('collapsed', value);
  }

  get currentKey() {
    return this.get('currentKey');
  }

  set currentKey(value: string) {
    this.set('currentKey', value);
  }

  get isAdmin() {
    return !!this.get('isAdmin');
  }

  set isAdmin(value: boolean) {
    this.set('isAdmin', value);
  }
}
