import * as React from 'react';
import * as collection from 'berish-collection';
import * as ringle from 'berish-ringle';

class ErrorController {
  private pages = new collection.Dictionary<
    string | number,
    React.ComponentClass
  >();

  DRegisterError(code: string | number) {
    let self = this;
    return function(Component: any) {
      self.registerErrorPage(Component, code);
      return Component;
    };
  }

  registerErrorPage(component: React.ComponentClass, code: string | number) {
    if (this.pages.containsKey(code)) return;
    this.pages.add(code, component);
  }

  getErrorPage(code: string | number) {
    if (this.pages.containsKey(code)) return this.pages.get(code);
    return null;
  }
}

export default ringle.getSingleton(ErrorController);
