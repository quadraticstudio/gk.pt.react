import messagesController from './messagesController';
import { ErrorTypeCode, getError, LangCode } from '../util/error';
import { IContentRouteProps } from '../router/contentRoute';
import storageController from './storageController';
import guid from 'berish-guid/dist';
import { NavigatorController } from './navigatorController';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';
import { EnumValueOf } from 'berish-enum/dist/lib';

const STOPPED_THREAD_EXECUTION = `STOPPED_THREAD_EXECUTION_${guid.guid()}`;

export interface IPageControllerTryLoad {
  title?: string;
  changeLoading?: (value: boolean) => any;
  disableLoading?: boolean;
  disableModalError?: boolean;
  allowException?: boolean;
  hash?: string;
}

export interface IPageControllerTryLoadNotification extends IPageControllerTryLoad {
  disableSuccessNotification?: boolean;
  titleNotification?: string;
  descriptionNotification?: string;
  typeNotification?: 'error' | 'success' | 'info' | 'warning';
}

export interface IPageControllerProps<MatchGeneric = {}, ResolveGeneric = {}> {
  controller: PageController<MatchGeneric, ResolveGeneric>;
  modal?: IStaticComponentProps<ResolveGeneric>;
}

export class PageController<MatchGeneric = {}, ResolveGeneric = {}> {
  private _navigatorController: NavigatorController<MatchGeneric, ResolveGeneric> = null;

  private executeListeners: {
    [hash: string]: () => void;
  } = {};

  constructor(router: IContentRouteProps<MatchGeneric, ResolveGeneric>) {
    this.receive(router);
  }

  receive(router: IContentRouteProps<MatchGeneric, ResolveGeneric>) {
    this._navigatorController = (this._navigatorController && this._navigatorController.receive(router)) || new NavigatorController(router);
    return this;
  }

  get navigator() {
    return this._navigatorController;
  }

  get storage() {
    return storageController;
  }

  async stopExecute(hash?: string) {
    let isSingle = !!hash;
    const stop = (hash: string) => {
      let listener = this.executeListeners[hash];
      if (listener) listener();
    };
    if (isSingle) stop(hash);
    else Object.keys(this.executeListeners).map(stop);
  }

  async tryLoad<T>(cb: (...args: any[]) => T | Promise<T>, config?: IPageControllerTryLoad, ...args: any[]) {
    let result: T = null;
    config = config || {};
    config.disableLoading = config.disableLoading || false;
    config.disableModalError = config.disableModalError || false;
    config.allowException = config.allowException || false;
    config.hash = config.hash || guid.generateId();

    let exception: Error = null;
    const execute = async () => {
      if (args && args.length > 0) return cb(...args);
      else return cb();
    };
    const checkExecute = () => {
      return new Promise<typeof STOPPED_THREAD_EXECUTION>((resolve, reject) => {
        this.executeListeners[config.hash] = () => {
          resolve(STOPPED_THREAD_EXECUTION);
          this.executeListeners[config.hash] = null;
        };
      });
    };
    try {
      if (!config.disableLoading) {
        if (config.changeLoading) await config.changeLoading(true);
        else {
          const systemStore = this.storage.systemStore;
          await systemStore.dispatch(
            systemStore.createMethod(m => {
              if (config.title) m.loadingTitle = config.title;
              let loadingHash = m.loadingHash || [];
              loadingHash.push(config.hash);
              m.loadingHash = loadingHash;
            })
          );
        }
      }
      let res = await Promise.race([execute(), checkExecute()]);
      if (res == STOPPED_THREAD_EXECUTION) throw new Error(STOPPED_THREAD_EXECUTION);
      result = res as T;
    } catch (err) {
      let mes = err && (err.message as string);
      if (mes != STOPPED_THREAD_EXECUTION) {
        if (process.env.NODE_ENV == 'development') console.error.call(this, err);
        if (!config.disableModalError) {
          messagesController.error(err);
        }
        if (config.allowException) {
          exception = err;
        }
      }
    } finally {
      if (!config.disableLoading) {
        if (config.changeLoading) await config.changeLoading(false);
        else {
          const systemStore = this.storage.systemStore;
          await systemStore.dispatch(
            systemStore.createMethod(m => {
              let loadingHash = m.loadingHash || [];
              if (config.title == m.loadingTitle) m.loadingTitle = null;
              let index = loadingHash.indexOf(config.hash);
              if (index != -1) loadingHash.splice(index, 1);
              m.loadingHash = loadingHash;
            })
          );
        }
      }
      if (exception) throw exception;
      return result;
    }
  }
  getError(code: EnumValueOf<typeof ErrorTypeCode>) {
    return getError(code, LangCode.RU);
  }

  async tryLoadNotification<T>(cb: (...args: any[]) => T | Promise<T>, config?: IPageControllerTryLoadNotification, ...args: any[]) {
    let result: T = null;
    config = config || {};
    config.disableSuccessNotification = config.disableSuccessNotification || false;
    config.typeNotification = config.typeNotification || 'success';
    config.titleNotification = config.titleNotification || config.title;
    config.descriptionNotification = config.descriptionNotification || 'Сохранение прошло успешно';
    config.disableModalError = config.disableModalError == null ? true : config.disableModalError;
    config.allowException = true;
    try {
      result = await this.tryLoad(cb, config, ...args);
      if (!config.disableSuccessNotification) messagesController.notification(config.typeNotification, config.titleNotification, config.descriptionNotification);
    } catch (err) {
      messagesController.error(err);
    } finally {
      return result;
    }
  }
}
