import * as ringle from 'berish-ringle';
import guid from 'berish-guid';
import { promiseGet, ObjectOrFunctionOrPromiseOrFunctionOfPromise } from 'berish-promise-getter';

const DEFAULT_TIMEOUT = 150;

interface IExecuteRaceResponse<T = any> {
  status: 'stop' | 'done' | 'error';
  result: T;
  error?: any;
}

function setTimeoutPromise(timeout: number) {
  return new Promise<void>(resolve => (timeout > 0 ? setTimeout(() => resolve(), timeout) : resolve()));
}

function executePromise<T>(config: IExecuteControllerConfig<T>, controller: ExecuteController, funcHash: string) {
  return new Promise<IExecuteRaceResponse<T>>(async resolve => {
    let response: IExecuteRaceResponse<T> = null;
    try {
      await setTimeoutPromise(config.timeout);
      let isExecute = controller.isExecute(funcHash);
      if (!isExecute) response = { status: 'stop', result: null };
      else {
        let result = await promiseGet(config.func);
        response = { status: 'done', result };
      }
    } catch (err) {
      response = { status: 'error', result: null, error: err };
    }
    delete controller.executeListeners[funcHash];
    resolve(response);
  });
}

export interface IExecuteControllerConfig<T> {
  func: ObjectOrFunctionOrPromiseOrFunctionOfPromise<T>;
  timeout?: number;
  setHash?: (funcHash: string, stopHash: string) => any;
  tag?: string;
  stopByTag?: boolean;
  debug?: boolean;
}

function getConfig<T>(config: IExecuteControllerConfig<T>) {
  const DEBUG = config.debug;
  const defaultConfig: IExecuteControllerConfig<T> = {
    func: DEBUG
      ? () => {
          console.log('it is test function executing');
          return null as T;
        }
      : () => {
          // EMPTY
          return null as T;
        },
    timeout: DEFAULT_TIMEOUT,
    setHash: DEBUG
      ? (funcHash, stopHash) => console.log(`funcHash ${funcHash}`, `stopHash ${stopHash}`)
      : () => {
          // EMPTY
        },
    tag: 'default'
  };
  return Object.assign({}, defaultConfig, config);
}

class ExecuteController {
  public executeListeners: { [hash: string]: () => void } = {};
  public tags: { [tag: string]: string[] } = {};

  private executeStopRace<T>(stopHash: string, funcHash: string) {
    return new Promise<IExecuteRaceResponse<T>>(resolve => {
      this.executeListeners[funcHash] = () => {
        delete this.executeListeners[funcHash];
        resolve({ status: 'stop', result: null });
      };
    });
  }

  async start<T>(config: IExecuteControllerConfig<T>) {
    config = getConfig(config);
    const stopHash = guid.guid();

    let funcHash = '';
    do funcHash = guid.guid();
    while (funcHash == stopHash);

    if (config.tag && config.stopByTag) {
      let tagArray = this.tags[config.tag] || [];
      let executeList = tagArray.map(m => this.isExecute(m));
      let isExecute = executeList.filter(m => !!m).length > 0;
      if (isExecute) this.stopAll(config.tag);
    }

    if (config.setHash) config.setHash(funcHash, stopHash);

    if (config.tag) {
      let tagArray = this.tags[config.tag] || [];
      tagArray.push(funcHash);
      this.tags[config.tag] = tagArray;
    }

    const res = await Promise.race([
      executePromise(config, this, funcHash),
      this.executeStopRace<T>(stopHash, funcHash)
    ]);

    if (config.setHash) config.setHash(null, stopHash);
    if (config.tag) {
      let tagArray = this.tags[config.tag] || [];
      let index = tagArray.indexOf(funcHash);
      if (index != -1) tagArray.splice(index, 1);
      this.tags[config.tag] = tagArray;
    }

    if (res.status == 'stop') return null;
    if (res.status == 'done') return res.result;
    throw res.error;
  }

  stop(hash: string) {
    let listener = this.executeListeners[hash];
    if (listener) {
      listener();
      return hash;
    }
    return null;
  }

  stopAll(tag?: string) {
    let hashes = Object.keys(this.executeListeners);
    if (tag) {
      let tagArray = this.tags[tag] || [];
      hashes = hashes.filter(m => tagArray.indexOf(m) != -1);
      this.tags[tag] = [];
    }
    return hashes.map(m => this.stop(m)).filter(m => m != null);
  }

  isExecute(funcHash: string | string[]) {
    const hashes = Array.isArray(funcHash) ? funcHash : [funcHash];
    const isExecute = hashes.map(m => !!this.executeListeners[m]).filter(m => m).length > 0;
    return isExecute;
  }
}

export default ringle.getSingleton(ExecuteController);
