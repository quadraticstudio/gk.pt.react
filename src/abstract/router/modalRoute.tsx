import * as React from 'react';
import { IPageControllerProps, PageController } from '../global/pageController';
import messagesController from '../global/messagesController';
import { IContentRouteProps } from './contentRoute';
import { ModalForm } from '../components';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';

interface IModalRouteState<MatchGeneric = {}, ResolveGeneric = {}> {
  loading: boolean;
  controller: PageController<MatchGeneric, ResolveGeneric>;
  unlistener: (() => void)[];
}

export default class ModalRoute<MatchGeneric = {}, ResolveGeneric = {}> extends React.PureComponent<
  IContentRouteProps<MatchGeneric, ResolveGeneric> & IStaticComponentProps<ResolveGeneric>,
  IModalRouteState<MatchGeneric, ResolveGeneric>
> {
  constructor(props) {
    super(props);
    this.state = {
      controller: null,
      unlistener: [],
      loading: true
    };
  }

  componentWillMount() {
    this.willMount();
  }

  initController = (nextProps?: IContentRouteProps<MatchGeneric, ResolveGeneric>) => {
    let controller = this.state.controller || new PageController(nextProps || this.props);
    let state = {};
    if (this.state.controller) {
      state = {
        controller: controller.receive(nextProps || this.props)
      };
    } else {
      const { systemStore, localStore, globalStore } = controller.storage;
      const systemUnlistener = systemStore.subscribe(m => this.forceUpdate());
      const localStoreUnlistener = localStore.subscribe(m => this.forceUpdate());
      const globalStoreUnlistener = globalStore.subscribe(m => this.forceUpdate());
      state = {
        controller,
        unlistener: [systemUnlistener, localStoreUnlistener, globalStoreUnlistener]
      };
    }
    this.setState(state);
  };

  willMount = async () => {
    try {
      await this.loading(true);
      this.initController();
    } catch (err) {
      messagesController.error(err);
    } finally {
      await this.loading(false);
    }
  };

  componentWillReceiveProps(nextProps: IContentRouteProps<MatchGeneric, ResolveGeneric>) {
    this.initController(nextProps);
  }

  componentWillUnmount() {
    this.willUnmount();
  }

  willUnmount = () => {
    if (this.state.unlistener) this.state.unlistener.map(m => m());
  };

  loading = (loading: boolean) => {
    return new Promise<void>(resolve => this.setState({ loading }, resolve));
  };

  resolve = (resolveGeneric: ResolveGeneric) => {
    this.props.resolve(resolveGeneric);
  };

  reject = () => {
    this.props.resolve();
  };

  renderAreaView = () => {
    let { controller } = this.state;
    let ComponentClass = this.props.component;
    return (
      <ModalForm resolve={this.resolve} reject={this.reject} width="80vw">
        <ComponentClass
          controller={controller}
          modal={{
            resolve: this.resolve,
            reject: this.reject,
            getContainer: this.props.getContainer
          }}
        />
      </ModalForm>
    );
  };

  render() {
    let loading = this.state.loading;
    if (loading) return messagesController.loadingJSXElement(<div style={{ width: '100vw', height: '100vh' }} />, loading);
    return this.renderAreaView();
  }
}
