import * as React from 'react';
import AreaView from '../content/areaView';
import { History } from 'history';
import { IPageControllerProps, PageController } from '../global/pageController';
import messagesController from '../global/messagesController';
import { getNotUsedAreaView } from '../util/decorators/route';

export interface IContentRouteProps<MatchGeneric = {}, ResolveGeneric = {}> {
  component: React.ComponentClass<IPageControllerProps>;
  params: MatchGeneric;
  history: History;
}

interface IContentRouteState {
  loading: boolean;
  controller: PageController;
  unlistener: (() => void)[];
}

export default class ContentRoute extends React.PureComponent<IContentRouteProps, IContentRouteState> {
  constructor(props) {
    super(props);
    this.state = {
      controller: null,
      unlistener: [],
      loading: true
    };
  }

  componentWillMount() {
    this.willMount();
  }

  initController = (nextProps?: IContentRouteProps) => {
    let controller = this.state.controller || new PageController(nextProps || this.props);
    let state = {};
    if (this.state.controller) {
      state = {
        controller: controller.receive(nextProps || this.props)
      };
    } else {
      const { systemStore, localStore, globalStore } = controller.storage;
      const systemUnlistener = systemStore.subscribe(m => {
        this.forceUpdate();
      });
      const localStoreUnlistener = localStore.subscribe(m => {
        this.forceUpdate();
      });
      const globalStoreUnlistener = globalStore.subscribe(m => {
        this.forceUpdate();
      });
      state = {
        controller,
        unlistener: [systemUnlistener, localStoreUnlistener, globalStoreUnlistener]
      };
    }
    this.setState(state);
  };

  willMount = async () => {
    try {
      await this.loading(true);
      this.initController();
    } catch (err) {
      messagesController.error(err);
    } finally {
      await this.loading(false);
    }
  };

  componentWillReceiveProps(nextProps: IContentRouteProps) {
    this.initController(nextProps);
  }

  componentWillUnmount() {
    this.willUnmount();
  }

  willUnmount = () => {
    if (this.state.unlistener) this.state.unlistener.map(m => m());
  };

  loading = (loading: boolean) => {
    return new Promise<void>(resolve => this.setState({ loading }, resolve));
  };

  renderAreaView = () => {
    let { controller } = this.state;
    let ComponentClass = this.props.component;
    let component = <ComponentClass controller={controller} />;
    let notUsedAreaView = getNotUsedAreaView(ComponentClass);
    return notUsedAreaView ? component : <AreaView controller={controller}>{component}</AreaView>;
  };

  render() {
    let loading = this.state.loading;
    if (loading) return messagesController.loadingJSXElement(<div style={{ width: '100vw', height: '100vh' }} />, loading);
    return this.renderAreaView();
  }
}
