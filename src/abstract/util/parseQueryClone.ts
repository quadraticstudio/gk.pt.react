import * as _ from 'lodash';

export default function parseQueryClone<T extends Parse.Object>(
  query: Parse.Query<T>
) {
  return _.cloneDeep(query);
}
