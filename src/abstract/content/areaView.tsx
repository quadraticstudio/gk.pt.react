import * as React from 'react';
import { Helmet } from 'react-helmet';

import HeaderView from './headerView';
import ContentView from './contentView';
import MenuView from './menuView';
import { IPageControllerProps } from '../global/pageController';
import { storageController, configController } from '../global';
import { LINQ } from 'berish-linq/dist';
import withStyles, { WithStylesProps } from './withStyles';

interface IAreaState {}

class AreaView extends React.Component<WithStylesProps<IPageControllerProps>, IAreaState> {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes } = this.props;
    let { title } = storageController.systemStore;
    return (
      <div>
        <Helmet defaultTitle={configController.meta.projectName} titleTemplate={`${configController.meta.projectName} - %s`}>
          <title>{title}</title>
        </Helmet>
        <div className={classes.root}>
          <HeaderView {...this.props} />
          <MenuView {...this.props} />
          <ContentView {...this.props} />
        </div>
      </div>
    );
  }
}

export default withStyles(AreaView);
