import * as React from 'react';
import { IPageControllerProps } from '../global/pageController';
import { configController } from '../global';

export default class FooterView extends React.Component<IPageControllerProps, any> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div
        style={{
          textAlign: 'center'
        }}
      >
        {configController.meta.projectName} © {new Date().getFullYear()}
      </div>
    );
  }
}
