import * as React from 'react';
import { AppBar, Toolbar, IconButton, Typography, Badge } from '@material-ui/core';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MenuIcon from '@material-ui/icons/Menu';
import * as classNames from 'classnames';
import { IPageControllerProps } from '../global/pageController';
import withStyles, { WithStylesProps } from './withStyles';

class HeaderView extends React.Component<WithStylesProps<IPageControllerProps>, {}> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onOpen = () => {
    const localStore = this.props.controller.storage.localStore;
    localStore.dispatch(
      localStore.createMethod(m => {
        m.collapsed = !m.collapsed;
      })
    );
  };

  render() {
    const { classes } = this.props;
    const { systemStore, localStore } = this.props.controller.storage;

    return (
      <AppBar position="absolute" className={classes.appBar}>
        <Toolbar disableGutters={true}>
          <IconButton color="inherit" aria-label="Открыть меню" onClick={() => this.onOpen()} className={classes.menuButton}>
            <MenuIcon />
          </IconButton>
          <Typography variant="title" color="inherit" noWrap className={classes.title}>
            {systemStore.title} {localStore.isAdmin ? '(Администратор)' : null}
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(HeaderView);
