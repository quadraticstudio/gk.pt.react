import { createStyles, withStyles, WithStyles } from '@material-ui/core';
import * as React from 'react';

export const drawerWidth = 240;
export let drawerClosedWidth = 64;

const styles = theme => {
  try {
    drawerClosedWidth = theme.spacing.unit * 7;
    return {
      root: {
        display: 'flex'
      },
      appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen
        })
      },
      appBarSpacer: theme.mixins.toolbar,
      contentContainer: {
        height: `calc(100vh - ${theme.mixins.toolbar.minHeight * 2}px)`
      },
      menuButton: {
        marginLeft: 12,
        marginRight: 36
      },
      notificationButton: {
        marginLeft: 36,
        marginRight: 12
      },
      menuButtonHidden: {
        display: 'none'
      },
      title: {
        flexGrow: 1
      },
      drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen
        })
      } as any,
      drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen
        }),
        width: drawerClosedWidth,
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing.unit * 9
        }
      } as any,
      drawerNestedItem: {
        marginLeft: '20px',
        transition: theme.transitions.create(['height', 'margin-left'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.enteringScreen
        }),
        height: '100px'
      },
      drawerNestedItemClose: {
        marginLeft: '0px',
        transition: theme.transitions.create(['height', 'margin-left'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen
        }),
        height: '0px'
      },
      content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        height: '100vh',
        overflow: 'auto'
      }
    };
  } catch (err) {
    return null;
  }
};

const tempObjectStylesType = styles(null);
type keyofStylesType = keyof typeof tempObjectStylesType;

export type WithStylesProps<T> = T & WithStyles<keyofStylesType>;
export default withStyles(styles);
