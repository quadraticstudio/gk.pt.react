import * as React from 'react';
import { IPageControllerProps } from '../global/pageController';
import withStyles, { WithStylesProps } from './withStyles';

class ContentView extends React.Component<WithStylesProps<IPageControllerProps>, any> {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes } = this.props;
    return (
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <div className={classes.contentContainer} style={{ overflowY: 'hidden' }}>
          <div style={{ height: '100%' }}>{this.props.children}</div>
        </div>
      </main>
    );
  }
}

export default withStyles(ContentView);
