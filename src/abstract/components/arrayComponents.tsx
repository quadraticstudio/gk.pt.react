import * as React from 'react';
import { LINQ } from 'berish-linq';

type elementType = JSX.Element | JSX.Element[];

interface IArrayComponentsProps<T> {
  template: (
    index?: number,
    object?: T,
    ...args: any[]
  ) => elementType | Promise<elementType>;
  elements: LINQ<T> | T[];
  parent?: JSX.Element;
  args?: any[];
}

interface IArrayComponentsState {}

function getLINQ<T>(linqOrArray: LINQ<T> | T[]) {
  if (linqOrArray instanceof LINQ) {
    return linqOrArray;
  }
  return LINQ.fromArray(linqOrArray || []);
}

export default class ArrayComponents<T> extends React.Component<
  IArrayComponentsProps<T>,
  IArrayComponentsState
> {
  static render<T>(props: IArrayComponentsProps<T>) {
    return this.renderTemplates(props);
  }

  static async renderAsync<T>(props: IArrayComponentsProps<T>) {
    return this.renderTemplatesAsync(props);
  }
  private static renderTemplates<T>(props: IArrayComponentsProps<T>) {
    let { elements, template, args } = props;
    return getLINQ(elements)
      .selectMany((m, i) => {
        let item = template(i, m, ...(args || [])) as
          | JSX.Element
          | JSX.Element[];
        return item instanceof Array ? item : [item];
      })
      .toArray();
  }

  private static async renderTemplatesAsync<T>(
    props: IArrayComponentsProps<T>
  ) {
    let { elements, template, args } = props;
    let temp = await Promise.all(
      getLINQ(elements)
        .select(async (m, i) => {
          let item = await (template(i, m, ...(args || [])) as Promise<
            JSX.Element | JSX.Element[]
          >);
          return item instanceof Array ? item : [item];
        })
        .toArray()
    );
    return getLINQ(temp)
      .selectMany(m => m)
      .toArray();
  }

  render() {
    let { parent } = this.props;
    let renderData = ArrayComponents.render(this.props);
    if (parent) return React.cloneElement(parent, {}, renderData);
    else {
      return <div>{renderData}</div>;
    }
  }

  private renderTemplates() {
    return ArrayComponents.renderTemplates(this.props);
  }
}
