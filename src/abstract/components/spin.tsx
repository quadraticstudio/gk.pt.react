import * as React from 'react';
import { CircularProgress } from '@material-ui/core';

export interface ISpinProps {
  loading?: boolean;
  style?: React.CSSProperties;
  className?: string;
}

export default class Spin extends React.Component<ISpinProps> {
  constructor(props: ISpinProps) {
    super(props);
  }

  render() {
    const { loading, style, className } = this.props;
    const defaultLoadingStyle = {
      position: 'absolute',
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      zIndex: 9999
    };
    const styleLoading = Object.assign({}, defaultLoadingStyle, style);
    return (
      <div style={{ position: 'relative', height: '100%', width: '100%' }}>
        {loading && (
          <div style={styleLoading} className={className}>
            <CircularProgress />
          </div>
        )}
        <div style={{ height: '100%', width: '100%' }}>{this.props.children}</div>
      </div>
    );
  }
}
