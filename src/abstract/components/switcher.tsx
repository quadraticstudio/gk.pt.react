import * as React from 'react';
import { Switch as MSwitch } from '@material-ui/core';
import { AbstractComponent, Tooltip, FormItem } from './abstract';

export interface ISwitcherProps {
  placeholder?: string;
  title?: string;
  disabled?: boolean;
}

class Switcher extends AbstractComponent<ISwitcherProps, {}> {
  constructor(props) {
    super(props);
  }

  onChange = (value: boolean) => {
    this.setValue(value);
  };

  render() {
    let value = this.getValue();
    let placeholder = this.props.placeholder || this.props.title;
    let input = (
      <MSwitch disabled={this.props.disabled} checked={value} onChange={e => this.onChange(e.target.checked)} />
    );
    return Tooltip(input, {
      placeholder: placeholder,
      trigger: 'hover'
    });
  }
}
export default FormItem(Switcher);
