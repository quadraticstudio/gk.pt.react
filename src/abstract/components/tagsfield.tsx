import * as React from 'react';
import { Select, MenuItem, Input, Icon, Chip } from '@material-ui/core';
import { LINQ } from 'berish-linq/dist';
import { AbstractComponent, Tooltip, FormItem } from './abstract';
import ArrayComponents from './arrayComponents';
import { ISelectFieldOption, ISelectFieldProps, getLinq } from './selectfield';

class Tagsfield<T, K> extends AbstractComponent<ISelectFieldProps<T, K>, {}> {
  constructor(props) {
    super(props);
  }

  onChange = (key: string[]) => {
    let data = this.convertValue(key, true);
    if (data instanceof Array) {
      let linq = LINQ.fromArray(data)
        .select(m => m.value)
        .toArray();
      this.setValue(linq);
    }
  };

  renderOption = (index: number, value: ISelectFieldOption<T>) => {
    return (
      <MenuItem key={index} value={`${index}`}>
        {value.view}
      </MenuItem>
    );
  };

  renderChip = (index: number, value: ISelectFieldOption<T>) => {
    return <Chip key={`${index}`} label={value.view} />;
  };

  convertValue(value: string[]): ISelectFieldOption<T>[];
  convertValue(
    value: string[],
    isOnChange: true
  ):
    | {
        type: 'string';
        data: string[];
      }
    | ISelectFieldOption<T>[];
  convertValue(value: string[], isOnChange?: boolean) {
    let linq = getLinq(this.props.data);
    let array = linq.toArray();
    let rdy = LINQ.fromArray(value)
      .select(m => {
        let parsed = parseInt(m, 10);
        if (isOnChange) {
          if (Number.isNaN(parsed)) return m;
        }
        return array[parsed || 0];
      })
      .notNull();
    let isStringAny = rdy.any(m => typeof m == 'string');
    if (isStringAny && isOnChange)
      return {
        type: 'string',
        data: rdy.where(m => typeof m == 'string').toArray() as string[]
      };
    return rdy.toArray() as ISelectFieldOption<T>[];
  }

  renderValue = (linq: LINQ<ISelectFieldOption<T>>) => {
    let { select } = this.props;
    let value = this.getValue();
    if (value == null) return null;
    let linqData: LINQ<any> = linq.select(m => m.value);
    if (select == null && linq.all(m => m.value && typeof m.value == 'object' && 'id' in m.value)) {
      select = data => {
        return data['id'];
      };
    }
    if (select != null) {
      value = LINQ.fromArray(value)
        .select(m => select(m as T))
        .toArray();
      linqData = linqData.select(select);
    }
    let indexes = LINQ.fromArray(value)
      .select(m => linqData.toArray().indexOf(m))
      .where(m => m != -1)
      .select(m => `${m}`)
      .toArray();
    return indexes;
  };

  renderSuffix = (value: any) => {
    return value && Array.isArray(value) && value.length > 0 ? (
      <Icon onClick={this.clearValue}>close-circle</Icon>
    ) : null;
  };

  clearValue = () => {
    this.onChange([]);
  };

  render() {
    let linq = getLinq(this.props.data);
    let value = this.renderValue(linq);
    const input = (
      <Select
        multiple={true}
        value={value != null ? value : []}
        onChange={e => this.onChange((e.target.value as any) as string[])}
        disabled={this.props.disabled}
        fullWidth={true}
        placeholder={this.props.placeholder}
        input={<Input />}
        endAdornment={!!this.props.disabled ? undefined : this.renderSuffix(value)}
      >
        {ArrayComponents.render({
          template: this.renderOption,
          elements: linq
        })}
      </Select>
    );
    return Tooltip(input, {
      placeholder: this.props.placeholder,
      trigger: 'click'
    });
  }
}

export default FormItem(Tagsfield, { fiTitleType: 'InputLabel' });
