import { createStyles, withStyles, WithStyles } from '@material-ui/core';
import * as React from 'react';

export const drawerWidth = 240;

const styles = theme => {
  try {
    return {
      paper: {
        position: 'absolute',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4
      } as any
    };
  } catch (err) {
    return null;
  }
};

const tempObjectStylesType = styles(null);
type keyofStylesType = keyof typeof tempObjectStylesType;

export type WithStylesProps<T> = T & WithStyles<keyofStylesType>;
export default <T>(componentClass: React.ComponentClass<T & WithStylesProps<T>>) => withStyles(styles)(componentClass as any) as React.ComponentClass<T>;
