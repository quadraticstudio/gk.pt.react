import * as React from 'react';
import { Modal } from '@material-ui/core';
import { messagesController, storageController } from '../../global';
import { IStaticComponentProps } from 'berish-react-portals/dist/lib/portal';
import { LINQ } from 'berish-linq/dist';
import Decorators from '../../util/decorators';
import withStyles, { WithStylesProps } from './withStyles';
import Spin from '../spin';

export interface IModalFormProps {
  width?: string | number;
  submit?: {
    validationObject: Object | Object[] | Object[][] | false;
    controller: React.Component;
  };
}

interface IModalFormState {
  loading: boolean;
}

class ModalForm extends React.Component<
  WithStylesProps<IModalFormProps> & IStaticComponentProps<null>,
  IModalFormState
> {
  unlistener = storageController.systemStore.subscribe(m => {
    let loading = !!(m.loadingHash && m.loadingHash.length > 0);
    if (loading != this.state.loading) this.setState({ loading });
  });

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  close = (func: () => any) => {
    if (func) func();
    if (this.unlistener) this.unlistener();
  };

  getValidations<T>(objects: T | T[] | T[][]) {
    if (!objects) {
      return false;
    }
    if (objects instanceof Array) {
      if (objects.length <= 0) {
        return [];
      }
      return LINQ.fromArray(objects as Array<any>)
        .selectMany(m => (m instanceof Array ? m : [m]))
        .notNull()
        .toArray();
    } else {
      return [objects];
    }
  }

  private onOk = () => {
    let submit: {
      validationObject: false | Object | Object[] | Object[][];
      controller: React.Component;
    } = this.props.submit || ({} as any);
    let validationObject: Object[] | false = this.getValidations(
      submit.validationObject
    );
    if (validationObject == false) {
      this.close(this.props.resolve);
    } else {
      let filters = LINQ.fromArray(validationObject || []).selectMany(m =>
        Decorators.Filter.Validate(m).toArray()
      );
      let error = filters.any(m => m.isError);
      if (error) {
        for (let model of filters.where(m => m.isError).toArray()) {
          model.isSetExecute = true;
        }
        {
          messagesController.notification(
            'error',
            'Не все поля корректны',
            'Заполните все поля корректно'
          );
        }
        if (submit.controller) submit.controller.forceUpdate();
      } else {
        this.close(this.props.resolve);
      }
    }
  };

  onCancel = () => {
    this.close(this.props.reject);
  };

  render() {
    const { classes } = this.props;
    return (
      <Modal
        open={true}
        title={undefined}
        container={
          this.props.getContainer ? this.props.getContainer(null) : null
        }
        onClose={e => this.props.reject && this.props.reject()}
      >
        <div
          style={{
            maxWidth: '90vw',
            maxHeight: '80vh',
            overflowY: 'auto',
            width: this.props.width,
            top: `50%`,
            left: `50%`,
            transform: `translate(-50%, -50%)`
          }}
          className={classes.paper}
        >
          <Spin loading={this.state.loading}>{this.props.children}</Spin>
        </div>
      </Modal>
    );
  }
}

export default withStyles(ModalForm);
