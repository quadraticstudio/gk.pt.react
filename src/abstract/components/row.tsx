import * as React from 'react';
import Divider from './divider';
import { FlexWrapProperty } from 'csstype';

export interface IRowProps {
  divider?: React.ReactNode;
  wrap?: FlexWrapProperty;
  style?: React.CSSProperties;
}

export default class extends React.Component<IRowProps> {
  renderWrapper = (content: React.ReactNode) =>
    React.Children.count(content) || this.props.style ? (
      <div
        style={{ display: 'flex', flexDirection: 'row', flexWrap: this.props.wrap || 'nowrap', ...this.props.style }}
      >
        {content}
      </div>
    ) : (
      content
    );
  render() {
    return (
      <>
        {this.props.divider && <Divider>{this.props.divider}</Divider>}
        {this.renderWrapper(this.props.children)}
      </>
    );
  }
}
