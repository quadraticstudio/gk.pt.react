import * as React from 'react';
import { Search } from '@material-ui/icons';
import { AbstractComponent, FormItem } from './abstract';
import { ITextFieldProps } from './textfield';
import { Row, Button, Col, Textfield } from './';

type objectOrPromise<T> = T | Promise<T>;

export interface ISearchFieldProps {
  placeholder?: string;
  onSearch?: (text: string) => objectOrPromise<void>;
  searchEverySymbol?: boolean;
}

export interface ISearchFieldState {
  text: string;
  searchText: string;
  loading: boolean;
}

class SearchField extends AbstractComponent<ISearchFieldProps & ITextFieldProps, ISearchFieldState> {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      searchText: '',
      loading: false
    };
  }

  onSeacrh = () => {
    this.loadData(this.search);
  };

  search = async () => {
    let { onSearch } = this.props;
    let { searchText, text } = this.state;
    if (searchText == text) return;
    if (!onSearch) return;
    let result = await onSearch(text);
    searchText = text;
    this.setState({ searchText });
  };

  async loadData<I>(promise: () => I | Promise<I>) {
    let loading = (loading: boolean) => new Promise<void>(resolve => this.setState({ loading }, resolve));
    let result: I = null;
    await loading(true);
    result = await promise();
    await loading(false);
    return result;
  }

  render() {
    const input = (
      <Textfield
        fiTitle="Поиск"
        config={{
          object: this.state,
          path: 'text',
          afterSet: m => {
            if (!m.text) {
              m.searchText = m.text;
              if (this.props.onSearch) this.props.onSearch(null);
            }
            this.setState(m, this.props.searchEverySymbol ? () => this.onSeacrh() : null);
          }
        }}
        placeholder={this.props.placeholder || 'Введите значение для поиска'}
        onPressEnter={this.onSeacrh}
      />
    );
    if (this.props.searchEverySymbol) return input;
    return (
      <Row>
        <Col span={23}>
          <Row>{input}</Row>
        </Col>
        <Col span={1}>
          <Button
            loading={this.state.loading}
            onClick={this.onSeacrh}
            icon={<Search />}
            shape="circle"
            placeholder="Начать поиск"
          />
        </Col>
      </Row>
    );
  }
}

export default FormItem(SearchField, { fiTitleType: 'InputLabel' });
