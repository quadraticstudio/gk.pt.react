import * as ringle from 'berish-ringle';
import guid from 'berish-guid';
import { FileWithPreview } from 'react-dropzone';

export interface IOnUploadConfig {
  formats?: string[];
  sizeMB?: number;
  extension?: string;
}

class UploadMethodsImplementation {
  async parse(file: FileWithPreview, config: IOnUploadConfig) {
    const Parse = await import('parse');
    if (!Parse) throw 'Нет способа загрузки. Необходимо описать способ загрузки';
    let fileParse = new Parse.File(`${guid.generateId()}.${config.extension}`, file);
    fileParse = await fileParse.save();
    return fileParse.url();
  }

  async base64(file: FileWithPreview) {
    return new Promise<string>((resolve, reject) => {
      let reader = new FileReader();
      reader.readAsDataURL(file as any);
      reader.onload = () => resolve(reader.result as string);
      reader.onerror = err => reject(err);
    });
  }

  promtDownload(base64: string, filename: string, mimeType: string) {
    let element = document.createElement('a');
    element.setAttribute('href', `data:${mimeType};base64,${base64}`);
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
}

export default ringle.getSingleton(UploadMethodsImplementation);
