import * as React from 'react';

export interface IRowProps {
  span?: number;
  style?: React.CSSProperties;
}

export default class extends React.Component<IRowProps> {
  render() {
    return <div style={{ display: 'flex', flex: this.props.span, flexDirection: 'column', ...this.props.style }}>{this.props.children}</div>;
  }
}
