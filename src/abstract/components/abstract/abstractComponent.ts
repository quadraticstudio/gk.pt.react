import * as React from 'react';
import { LINQ } from 'berish-linq';
import * as accessors from 'berish-accessors-string';
import { ObjectOrFunctionOrPromiseOrFunctionOfPromise, promiseGet } from 'berish-promise-getter';

interface IConfigPropsObject {
  object: any;
  path?: string;
  beforeGet?: (object: any, path: string) => ObjectOrFunctionOrPromiseOrFunctionOfPromise<void>;
  afterGet?: (object: any, path: string, value: any) => any;
  beforeSet?: (object: any, path: string, value: any) => ObjectOrFunctionOrPromiseOrFunctionOfPromise<any>;
  afterSet?: (object: any, path: string, value: any) => ObjectOrFunctionOrPromiseOrFunctionOfPromise<void>;
}

type ConfigPropsWithType = IConfigPropsObject & { type: number };

export interface IAbstractComponentProps {
  config?: IConfigPropsObject;
  configs?: ConfigPropsWithType[];
}

export class AbstractComponent<P = {}, S = {}> extends React.Component<P & IAbstractComponentProps, S> {
  private getConfig(attribute?: number, props?: P & IAbstractComponentProps) {
    props = props || (this.props as any);
    let configProps = props.config;
    let configsProps = props.configs;
    let config: ConfigPropsWithType = null;
    if (attribute == null) {
      if (configsProps) {
        let linq = LINQ.fromArray(configsProps || []);
        let count = linq.count();
        if (count == 1) config = configProps[0];
        else {
          config = linq.single(m => m.type == null);
        }
      } else {
        config = configProps as ConfigPropsWithType;
      }
    } else {
      if (configsProps) {
        let linq = LINQ.fromArray(configsProps || []);
        config = linq.single(m => m.type == attribute);
      } else {
        throw new Error('you trying to get value by attribute - index of Array, but got Object in AbstractComponent');
      }
    }
    return config || {} as IConfigPropsObject;
  }

  get config() {
    try {
      return this.getConfig();
    } catch (err) {
      return null;
    }
  }

  async setValue(value: any, attrName?: number, props?: P & IAbstractComponentProps) {
    let config = this.getConfig(attrName, props);
    if (config.beforeSet) value = await promiseGet(config.beforeSet(config.object, config.path, value));
    accessors.setValue(config.object, config.path, value);
    if (config.afterSet) await promiseGet(config.afterSet(config.object, config.path, value));
  }

  getValue(attrName?: number, props?: P & IAbstractComponentProps) {
    let config = this.getConfig(attrName, props);
    if (config.beforeGet) config.beforeGet(config.object, config.path);
    let value = accessors.getValue(config.object, config.path);
    if (config.afterGet) value = config.afterGet(config.object, config.path, value);
    return value;
  }
}
