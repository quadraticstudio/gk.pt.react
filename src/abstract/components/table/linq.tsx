import AbstractTable, {
  IAbstractTableProps,
  IAbstractTableState,
  ON_PAGE_DEFAULT
} from './abstract';
import { LINQ } from 'berish-linq/dist';
import { IColumn } from './view';

type objectOrPromise<T> = T | Promise<T>;
type linqOrArray<T> = LINQ<T> | T[];

function getLinq<T>(linqArray: linqOrArray<T>) {
  if (linqArray instanceof LINQ) return linqArray;
  return LINQ.fromArray(linqArray || []);
}

interface ILINQTableProps<T>
  extends IAbstractTableProps<objectOrPromise<linqOrArray<T>>> {}
interface ILINQTableState<T> extends IAbstractTableState<T> {
  allData: LINQ<T>;
}

export default class LINQTable<T> extends AbstractTable<
  ILINQTableProps<T>,
  ILINQTableState<T>
> {
  constructor(props: ILINQTableProps<T>) {
    super(props);
    this.state = {
      ...this.state,
      allData: LINQ.fromArray([])
    };
  }

  load = async (nextProps?: ILINQTableProps<T>) => {
    let { data } = nextProps || this.props;
    if (data == null) return;
    let { allData, showData, total, currentPage } = this.state;
    currentPage = 1;
    let temp = await data;
    allData = getLinq(temp);
    total = allData.count();
    showData = await this.loadPage(currentPage, nextProps, null, {
      ...this.state,
      allData
    });
    this.setState({
      allData,
      showData,
      total,
      currentPage
    });
  };

  loadPage = async (
    page: number,
    nextProps?: ILINQTableProps<T>,
    pageSize?: number,
    state?: ILINQTableState<T>
  ) => {
    let { onPage } = nextProps || this.props;
    let { showData } = this.state;
    let limit = pageSize || onPage || ON_PAGE_DEFAULT;
    let queryData = state.allData.skip(limit * (page - 1)).take(limit);
    showData = queryData.toArray();
    return showData;
  };

  sortPage = async (column: IColumn<any>, type: 'down' | 'up') => {
    let { allData, showData, currentPage } = this.state;
    let renders = allData.select((m, i) => ({
      render: column.sorter ? column.sorter(m, i) : column.render(m, i),
      data: m
    }));
    if (type == 'down') renders = renders.orderBy(m => m.render);
    else if (type == 'up') renders = renders.orderByDescending(m => m.render);
    allData = renders.select(m => m.data);
    showData = await this.loadPage(currentPage, null, null, {
      ...this.state,
      allData
    });
    this.setState({ allData, showData });
  };
}
