import * as React from 'react';
import Decorators from 'berish-decorate';
import * as Parse from 'parse';
import * as Model from '../../../model';
import {
  DRouteQuery,
  DPublicRoute
} from '../../../abstract/util/decorators/route';
import { IPageControllerProps } from '../../../abstract/global/pageController';

import View from './view';

export interface IControllerParams {
  id?: string;
}

export interface IControllerProps
  extends IPageControllerProps<IControllerParams, Model.TeamUser> {}

interface IControllerState {
  item: Model.TeamUser;
}

export class Controller extends React.Component<
  IControllerProps,
  IControllerState
> {
  constructor(props: IControllerProps) {
    super(props);
    this.state = {
      item: new Model.TeamUser()
    };
  }

  componentDidMount() {
    this.props.controller.tryLoad(this.load);
  }

  load = async () => {
    let { id } = this.props.controller.navigator.params;
    let { item } = this.state;
    if (id)
      item = await new Parse.Query(Model.TeamUser)
        .include(['team', 'projects'])
        .get(id);
    this.setState({ item });
  };

  save = async () => {
    let { item } = this.state;
    item = await item.save();
    this.setState({ item });
    if (this.props.modal) return this.props.modal.resolve(item);
    else
      return this.props.controller.navigator.replace(`/teamuser/form`, {
        id: item.id
      });
  };

  cancel = () => {
    if (this.props.modal) return this.props.modal.reject();
    else return this.props.controller.navigator.replace(`/teamuser/list`);
  };

  // VIEW

  onChange = (item: Model.TeamUser) => this.setState({ item });
  onSave = () => this.props.controller.tryLoadNotification(this.save);
  onCancel = () => this.props.controller.tryLoad(this.cancel);

  render() {
    return <View controller={this} />;
  }
}

export default Decorators.classDecorate(Controller, [
  DRouteQuery(DPublicRoute())
]);
