import * as React from 'react';
import Decorators from 'berish-decorate';
import { IPageControllerProps } from '../../abstract/global/pageController';
import { messagesController } from '../../abstract/global';
import {
  DRouteQuery,
  DPublicRoute
} from '../../abstract/util/decorators/route';

interface IControllerProps extends IPageControllerProps {}

interface IControllerState {}

class MainViewController extends React.Component<
  IControllerProps,
  IControllerState
> {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.controller.tryLoad(this.load);
  }

  load = async () => {
    this.pushList();
  };

  pushList = () => {
    this.props.controller.navigator.push('/teamuser/list');
  };

  render() {
    return messagesController.loadingGlobal(true);
  }
}

export default Decorators.classDecorate(MainViewController, [
  DRouteQuery(DPublicRoute())
]);
