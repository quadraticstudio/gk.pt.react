import * as React from 'react';
import Decorators from 'berish-decorate';
import * as Parse from 'parse';
import * as Model from '../../../model';
import Form from '../form/controller';
import {
  DRouteQuery,
  DPublicRoute
} from '../../../abstract/util/decorators/route';
import { IPageControllerProps } from '../../../abstract/global/pageController';

import View from './view';

export interface IControllerProps extends IPageControllerProps {}

interface IControllerState {
  query: Parse.Query<Model.Project>;
  selected: Model.Project[];
}

export class Controller extends React.Component<
  IControllerProps,
  IControllerState
> {
  constructor(props: IControllerProps) {
    super(props);
    this.state = {
      query: this.getQuery(),
      selected: []
    };
  }

  getQuery = (query?: Parse.Query<Model.Project>) => {
    query = query || new Parse.Query(Model.Project);
    return query;
  };

  componentDidMount() {
    this.props.controller.tryLoad(this.load);
  }

  load = async () => {
    const systemStore = this.props.controller.storage.systemStore;
    await systemStore.dispatch(
      systemStore.createMethod(m => {
        m.title = 'Проекты';
      })
    );
    let { query } = this.state;
    query = this.getQuery();
    this.setState({ query });
  };

  remove = async () => {
    let { selected } = this.state;
    await Promise.all(selected.map(m => m.destroy()));
  };

  // VIEW

  onSelect = (selected: Model.Project[]) => this.setState({ selected });

  onAdd = async () => {
    let item = await this.props.controller.navigator.pushModal(Form)();
    if (item) this.setState({ query: this.getQuery() });
  };
  onEdit = async () => {
    let item = await this.props.controller.navigator.pushModal(Form)({
      id: this.state.selected[0].id
    });
    if (item) this.setState({ query: this.getQuery() });
  };
  onRemove = async () => {
    await this.props.controller.tryLoad(this.remove);
    this.setState({ query: this.getQuery(), selected: [] });
  };

  render() {
    return <View controller={this} />;
  }
}

export default Decorators.classDecorate(Controller, [
  DRouteQuery(DPublicRoute())
]);
