import * as React from 'react';
import * as Model from '../../../model';
import * as Components from '../../../abstract/components';
import { IColumn } from '../../../abstract/components/table/view';
import { Controller } from './controller';

export interface IViewProps {
  controller: Controller;
}

export default class extends React.Component<IViewProps> {
  constructor(props: IViewProps) {
    super(props);
  }

  renderColumns = () => {
    let columns: IColumn<Model.Team>[] = [
      {
        title: 'Название отдела',
        render: item => item.name
      },
      {
        title: 'Полное наименование отдела',
        render: item => item.fullname
      }
    ];
    return columns;
  };

  renderHeader = () => {
    let { state, onAdd, onEdit, onRemove } = this.props.controller;
    let { selected } = state;
    let left = [
      <Components.Button key="1" buttonTitle="Добавить" onClick={onAdd} />,
      <Components.Button
        key="2"
        buttonTitle="Редактировать"
        disabled={selected.length != 1}
        onClick={onEdit}
      />,
      <Components.Button
        key="3"
        buttonTitle="Удалить"
        type="secondary"
        disabled={selected.length < 1}
        onClick={onRemove}
      />
    ];
    return <Components.ButtonBar left={left} />;
  };
  render() {
    const isAdmin = this.props.controller.props.controller.storage.localStore
      .isAdmin;
    let { state, onSelect } = this.props.controller;
    let { query, selected } = state;
    return (
      <Components.ContentForm headerBar={isAdmin ? this.renderHeader() : null}>
        <Components.Table.QueryTable
          data={query}
          columns={this.renderColumns()}
          selection={{ items: selected, on: onSelect }}
          columnStyle={{ fontSize: 19 }}
          rowStyle={{ fontSize: 17 }}
        />
      </Components.ContentForm>
    );
  }
}
