import * as React from 'react';
import * as Components from '../../../abstract/components';
import { Controller } from './controller';

export interface IViewProps {
  controller: Controller;
}

export default class extends React.Component<IViewProps> {
  renderContent = () => {
    let { state, onChange } = this.props.controller;
    let { item } = state;
    return (
      <>
        <Components.Textfield
          fiTitle="Наименование отдела"
          config={{
            object: item,
            path: 'name',
            afterSet: onChange
          }}
        />
        <Components.Textfield
          fiTitle="Полное наименование отдела"
          config={{
            object: item,
            path: 'fullname',
            afterSet: onChange
          }}
        />
      </>
    );
  };

  renderHeader = () => {
    let { state, onCancel, onSave } = this.props.controller;
    let left = [
      <Components.Button
        key="1"
        buttonTitle="Сохранить"
        onClick={onSave}
        submit={{ validationObject: state.item, controller: this }}
      />,
      <Components.Button
        key="2"
        buttonTitle="Отменить"
        onClick={onCancel}
        type="secondary"
      />
    ];
    return <Components.ButtonBar left={left} />;
  };

  render() {
    return (
      <Components.ContentForm headerBar={this.renderHeader()}>
        {this.renderContent()}
      </Components.ContentForm>
    );
  }
}
