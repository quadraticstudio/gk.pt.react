import * as Parse from 'parse';
import Decorators from 'berish-decorate';
import { IsRequired } from '../abstract/util/decorators/filter';

export class Team extends Parse.Object {
  constructor() {
    super('Team');
  }

  get name() {
    return this.get('name');
  }

  set name(value: string) {
    this.set('name', value);
  }

  get fullname() {
    return this.get('fullname');
  }

  set fullname(value: string) {
    this.set('fullname', value);
  }
}

Decorators.methodDecorate(Team, 'name', [IsRequired()]);

Parse.Object.registerSubclass('Team', Team);
